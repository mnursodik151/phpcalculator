<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;

class CalculatorController
{
    public function calculate(Request $request)
    {
        $result = shell_exec(
            sprintf('php app %s %s', $request->Route()->parameters['action'], implode(' ', $request->post()['input'] ) )
        );

        $resultSplit = explode("=", str_replace("\n", '', $result ));

        $json = array(
            "command" => $request->Route()->parameters['action'],
            "operation" => $resultSplit[0],
            "result" => $resultSplit[1]
        );

        return json_encode($json);
    }
}
