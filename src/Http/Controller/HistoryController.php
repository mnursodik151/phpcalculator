<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class HistoryController
{
    public function index(Request $request, ResponseFactory $response)
    {
        // todo: modify codes to get history
        echo $request->query('driver');
    }

    public function show()
    {
        dd('create show history by id here');
    }

    public function remove()
    {
        // todo: modify codes to remove history
        dd('create remove history logic here');
    }
}
