<?php

namespace Jakmall\Recruitment\Calculator\History;

class CommandHistoryLogItem
{
    public $id;
    public $command;
    public $operation;
    public $result;

    public function __construct( int $id, $command, $operation, $result )
    {
        $this->id = $id;
        $this->command = $command;
        $this->operation = $operation;
        $this->result = $result;
    }

    public function getItem()
    {
        return array(
            "id" => $this->id,
            "command" => $this->command,
            "operation" => $this->operation,
            "result" => $this->result,
        );
    }
}