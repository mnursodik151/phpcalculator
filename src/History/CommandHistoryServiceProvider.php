<?php

namespace Jakmall\Recruitment\Calculator\History;

use Illuminate\Contracts\Container\Container;
use Jakmall\Recruitment\Calculator\Container\ContainerServiceProviderInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\FileCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\LatestCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\CompositeCommandHistoryManager;

class CommandHistoryServiceProvider implements ContainerServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $container): void
    {
        $container->bind( FileCommandHistoryManager::class, function() {
            return new FileCommandHistoryManager();
        });

        $container->bind( LatestCommandHistoryManager::class, function() {
            return new LatestCommandHistoryManager();
        });

        $container->bind( CompositeCommandHistoryManager::class, function() {
            return new CompositeCommandHistoryManager();
        });

        $container->tag(
            [
                "file" => FileCommandHistoryManager::class, 
                "latest" => LatestCommandHistoryManager::class, 
                "composite" => CompositeCommandHistoryManager::class,
            ], 
            'drivers'
        );

        $container->bind(
            CommandHistoryManagerInterface::class,
            function ($container) {
                //todo: register implementation 
                return new CompositeCommandHistoryManager();
            }
        );
    }
}
