<?php
namespace Jakmall\Recruitment\Calculator\History;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

//TODO: create implementation.
class LatestCommandHistoryManager implements CommandHistoryManagerInterface
{
    private $root;
    private $fileName;

    public function __construct()
    {
        $this->root = dirname(__DIR__, 2);
        $this->fileName = '/storage/latest.log';
    }
    /**
     * Returns array of command history.
     *
     * @return array returns an array of commands in storage
     */
    public function findAll(): array
    {
        $logItems = array();

        $logFile = fopen($this->root.$this->fileName, "r") or die("Unable to open file!");
        while(!feof($logFile)) {
            $data = fgets($logFile);            
            $logDataElements = explode(";", $data);
            $logItem = new CommandHistoryLogItem( 
                (int)$logDataElements[0], $logDataElements[1], $logDataElements[2], $logDataElements[3]
            );

            if($logItem->id != null)
            {
                array_push($logItems, $logItem );
            }
        }
        fclose($logFile);

        return $logItems;
    }

    /**
     * Find a command by id.
     *
     * @param string|int $id
     *
     * @return null|mixed returns null when id not found.
     */
    public function find($id)
    {
        $logItems = $this->findAll();
        
        foreach( $logItems as $logItem )
        {
            if ( $logItem->id === $id) 
            {    
                $result = $logItem;
                $index = array_search( $logItem, $logItems );

                array_splice( $logItems, $index, 1 );
            
                $logFile = fopen($this->root.$this->fileName, "w") or die("Unable to open file!");
                foreach($logItems as $writeItem)
                {
                    fwrite($logFile, implode(";", $writeItem->getItem()));
                }
                fclose($logFile);           

                return $result;
            }
        }
        return null;
    }

    /**
     * Find a command by operation.
     *
     * @param string $operation
     *
     * @return null|mixed returns null when id not found.
     */
    private function findByOperation($operation)
    {
        $logItems = $this->findAll();
        
        foreach( $logItems as $logItem )
        {
            if ( $logItem->operation === $operation) 
            {
                return $logItem;
            }
        }
        return null;
    }

    /**
     * Log command data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command): bool
    {
        $logItems = $this->findAll();
        
        array_unshift( $command, count($logItems) > 0 ? end($logItems)->id + 1 : 1 );
        $lineBreak = count($logItems) > 0 ? "\n" : '';

        $loggedCommand = $this->findByOperation($command["operation"]);

        if($loggedCommand != null)
        {
            $this->clear($loggedCommand->id);
            $logItems = $this->findAll();
        }

        if( count($logItems) > 9 )
        {
            $firstItem = reset($logItems);
            $this->clear($firstItem->id);
        }
        
        try
        {
            $logFile = fopen($this->root.$this->fileName, "a") or die("Unable to open file!");
            fwrite($logFile, $lineBreak.implode(";", $command));
            fclose($logFile);
            return true;
        }
        catch( throwable $e )
        {
            return false;
        }
    }

    /**
     * Clear a command by id
     *
     * @param string|int $id
     *
     * @return bool Returns true when data with $id is cleared successfully, false otherwise.
     */
    public function clear($id): bool
    {
        $item = $this->find($id);

        if($item != null)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll():bool
    {
        try
        {
            $logFile = fopen($this->root.$this->fileName, "w") or die("Unable to open file!");
            fwrite($logFile, "");
            fclose($logFile);
            return true;
        }
        catch( throwable $e )
        {
            return false;
        }
    }
}
