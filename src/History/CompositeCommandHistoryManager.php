<?php

namespace Jakmall\Recruitment\Calculator\History;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
// use Jakmall\Recruitment\Calculator\History\CommandHistoryLogItem;

//TODO: create implementation.
class CompositeCommandHistoryManager implements CommandHistoryManagerInterface
{
    private $root;
    private $FileDriver;
    private $LatestDriver;

    public function __construct()
    {
        $this->root = dirname(__DIR__, 2);
        $this->FileDriver = new FileCommandHistoryManager();
        $this->LatestDriver = new LatestCommandHistoryManager();

    }
    /**
     * Returns array of command history.
     *
     * @return array returns an array of commands in storage
     */
    public function findAll(): array
    {
        return $this->FileDriver->findAll();
    }

    /**
     * Find a command by id.
     *
     * @param string|int $id
     *
     * @return null|mixed returns null when id not found.
     */
    public function find($id)
    {
        $logItem = $this->LatestDriver->find($id);

        if($logItem != null)
        {
            return $logItem;
        }
        else
        {
            return $this->FileDriver->find($id);
        }
    }

    /**
     * Log command data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command): bool
    {
        $file = $this->FileDriver->log($command);
        $latest = $this->LatestDriver->log($command);

        return $file || $latest;
    }

    /**
     * Clear a command by id
     *
     * @param string|int $id
     *
     * @return bool Returns true when data with $id is cleared successfully, false otherwise.
     */
    public function clear($id): bool
    {
        $file = $this->FileDriver->clear($id);
        $latest = $this->LatestDriver->clear($id);

        return $file || $latest;
    }

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll():bool
    {
        $file = $this->FileDriver->clearAll();
        $latest = $this->LatestDriver->clearAll();

        return $file || $latest;
    }
}
