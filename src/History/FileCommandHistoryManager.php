<?php

namespace Jakmall\Recruitment\Calculator\History;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
// use Jakmall\Recruitment\Calculator\History\CommandHistoryLogItem;

//TODO: create implementation.
class FileCommandHistoryManager implements CommandHistoryManagerInterface
{
    private $root;
    private $fileName;

    public function __construct()
    {
        $this->root = dirname(__DIR__, 2);
        $this->fileName = '/storage/mesinhitung.log';
    }
    /**
     * Returns array of command history.
     *
     * @return array returns an array of commands in storage
     */
    public function findAll(): array
    {
        $logItems = array();

        $logFile = fopen($this->root.$this->fileName, "r") or die("Unable to open file!");
        while(!feof($logFile)) {
            $data = fgets($logFile);            
            $logDataElements = explode(";", $data);
            $logItem = new CommandHistoryLogItem( 
                (int)$logDataElements[0], $logDataElements[1], $logDataElements[2], $logDataElements[3]
            );

            if($logItem != null)
            {
                array_push($logItems, $logItem );
            }
        }
        fclose($logFile);

        return $logItems;
    }

    /**
     * Find a command by id.
     *
     * @param string|int $id
     *
     * @return null|mixed returns null when id not found.
     */
    public function find($id)
    {
        $logItems = $this->findAll();
        
        foreach( $logItems as $logItem )
        {
            if ( $logItem->id === $id) 
            {
                return $logItem;
            }
        }
        return null;
    }

    /**
     * Log command data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command): bool
    {
        $logItems = $this->findAll();
        array_unshift( $command, count($logItems) + 1 );
        $lineBreak = count($logItems) > 0 ? "\n" : '';

        try
        {
            $logFile = fopen($this->root.$this->fileName, "a") or die("Unable to open file!");
            fwrite($logFile, $lineBreak.implode(";", $command));
            fclose($logFile);
            return true;
        }
        catch( throwable $e )
        {
            return false;
        }
    }

    /**
     * Clear a command by id
     *
     * @param string|int $id
     *
     * @return bool Returns true when data with $id is cleared successfully, false otherwise.
     */
    public function clear($id): bool
    {
        $logItems = $this->findAll();

        try
        {
            $index = array_search( $this->find($id), $logItems );

            if( $index !== false)
            {
                array_splice( $logItems, $index, 1 );
            }
            else
            {
                return false;
            }
        
            $logFile = fopen($this->root.$this->fileName, "w") or die("Unable to open file!");
            foreach($logItems as $logItem)
            {
                fwrite($logFile, implode(";", $logItem->getItem()));
            }
            fclose($logFile);
            return true;
        }
        catch( throwable $e )
        {
            return false;
        }
    }

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll():bool
    {
        try
        {
            $logFile = fopen($this->root.$this->fileName, "w") or die("Unable to open file!");
            fwrite($logFile, "");
            fclose($logFile);
            return true;
        }
        catch( throwable $e )
        {
            return false;
        }
    }
}
