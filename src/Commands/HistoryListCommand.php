<?php

namespace Jakmall\Recruitment\Calculator\Commands;
use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\FileCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\LatestCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\CompositeCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\CommandHistoryLogItem;

class HistoryListCommand extends Command {
    
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;  

    private $FileDriver;
    private $LatestDriver;
    private $CompositeDriver;

    public function __construct( FileCommandHistoryManager $File, LatestCommandHistoryManager $Latest, CompositeCommandHistoryManager $Composite )
    {   
        $this->FileDriver = $File;
        $this->LatestDriver = $Latest;
        $this->CompositeDriver = $Composite;

        $this->signature = 'history:list {id? : The id of the log item} {--driver=}';
        $this->description = 'list log items stored in file';
        
        parent::__construct();
    }

    public function handle(): void
    {
        $option = $this->option("driver");
        $argument = $this->argument("id");
        $driver = $this->CompositeDriver;

        switch($option) 
        {
            case "file" :
                $driver = $this->FileDriver;
            break;
            case "latest" :
                $driver = $this->LatestDriver;
            break;
        }
        
        $header = array("id", "command", "operation", "result");
        $datas = array();

        if( isset($argument) )
        {
            $item = $driver->find((int)$argument);
            if( $item != null)
            {
                array_push($datas, $item->getItem());
            }
            else
            {
                array_push($datas, array(0,0,0,0));
            }
        }
        else
        {                 
            foreach( $driver->findAll() as $logItem)
            {   
                array_push($datas, $logItem->getItem());
            }            
        }
        
        var_dump($datas);
        $this->table($header, $datas);
        
    }
}
