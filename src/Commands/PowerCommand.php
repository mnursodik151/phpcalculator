<?php

namespace Jakmall\Recruitment\Calculator\Commands;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class PowerCommand extends CalculatorCommands {
    
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;    

    protected $commandHistoryManager;

    public function __construct( CommandHistoryManagerInterface $manager )
    {
        parent::__construct();
        $this->initializeCommandHistoryManager($manager);
    }

    protected function getCommandVerb(): string
    {
        return 'power';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'power';
    }

    protected function getOperator(): string
    {
        return '^';
    } 

    public function handle(): void
    {
        $numbers = $this->getInput();
        if( count($numbers) <= 2 )
        {
            parent::handle();
        }
        else 
        {
            $this->error("argument missmatch : power only accept two arguments");
        }
    }
     
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        $result = 1;
        for( $i = 0; $i < $number2; $i++ )
        {
            $result *= $number1; 
        }
        return $result;
    }
}
