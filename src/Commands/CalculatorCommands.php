<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

abstract class CalculatorCommands extends Command {
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */

    protected $commandHistoryManager;

    public function __construct()
    {      
        $commandVerb = $this->getCommandVerb();
        
        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($commandVerb));
        
        parent::__construct();
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        $logged = $this->logCommand( 
            array( "command" => $this->getCommandVerb(), "operation" => $description, "result" => $result ) 
        );
        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function initializeCommandHistoryManager( CommandHistoryManagerInterface $manager ): void 
    {        
        $this->$commandHistoryManager = $manager;
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function logCommand($command)
    {
        if( $this->$commandHistoryManager != null )
        {
            return $this->$commandHistoryManager->log($command);
        }
        else 
        {
            $this->error("not found");
            return false;
        }
    }

    protected abstract function getOperator():string;
    protected abstract function getCommandVerb():string;
    protected abstract function getCommandPassiveVerb():string;

    /**
     * @param array $numbers
     *
     * @return string
     */
    protected function generateCalculationDescription(array $numbers): string
    {
        $glue = sprintf(' %s ', $this->getOperator());

        return implode($glue, $numbers);
    }  

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected abstract function calculate($number1, $number2);
}