<?php

namespace Jakmall\Recruitment\Calculator\Commands;
use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\FileCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\LatestCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\CompositeCommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\CommandHistoryLogItem;

class HistoryClearCommand extends Command {
    
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;  

    private $CompositeDriver;

    public function __construct( CompositeCommandHistoryManager $Composite )
    {   
        $this->CompositeDriver = $Composite;

        $this->signature = 'history:clear {id? : The id of the log item}';
        $this->description = 'delete log items stored in file';
        
        parent::__construct();
    }

    public function handle(): void
    {
        $argument = $this->argument("id");
        $driver = $this->CompositeDriver;

        if( isset($argument) )
        {
            if( $driver->clear((int) $argument ) )
            {
                $this->comment(sprintf('Data with ID %s is removed', $argument));
            }
            else
            {
                $this->error('Failed to remove data');
            }
        }
        else
        {                 
            if( $driver->clear() )
            {
                $this->comment('All history is cleared');
            }
            else
            {
                $this->error('Failed to clear history');
            }          
        }
        
    }
}
