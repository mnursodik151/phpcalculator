<?php

namespace Jakmall\Recruitment\Calculator\Commands;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class DivideCommand extends CalculatorCommands {
    
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;    

    protected $commandHistoryManager;

    public function __construct( CommandHistoryManagerInterface $manager )
    {        
        parent::__construct();
        $this->initializeCommandHistoryManager($manager);
    }

    protected function getCommandVerb(): string
    {
        return 'divide';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'divided';
    }

    protected function getOperator(): string
    {
        return '/';
    } 
     
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 / $number2;
    }
}
